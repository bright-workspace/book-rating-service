package com.brightinventions.books.fixtures;

import com.brightinventions.books.model.Book;

public class BookFixtures {

    public static final String INCORRECT_ISBN = "0-545-01022-";
    public static final String CORRECT_ISBN_13 = "978-3-16-148410-0";
    public static final String CORRECT_ISBN_10 = "0-545-01022-1";

    public static Book getSimpleBook(){
        return Book.builder()
                .author("Cheeseburger Lord")
                .isbn(CORRECT_ISBN_13)
                .pages(100)
                .rating(3)
                .title("Lord of the Cheeseburgers")
                .build();
    }
}
