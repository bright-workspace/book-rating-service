package com.brightinventions.books.controller;

import com.brightinventions.books.model.Book;
import com.brightinventions.books.service.BookService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static com.brightinventions.books.fixtures.BookFixtures.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(BookController.class)
class BookControllerTest {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private BookService bookService;

    @AfterEach
    void tearDown() {
        Mockito.reset(bookService);
    }

    @Test
    void should_FindAllBooks() throws Exception {
        mockMvc.perform(get("/books").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @ParameterizedTest
    @ValueSource(strings = {
            CORRECT_ISBN_13,
            CORRECT_ISBN_10
    })
    void should_SaveBookWithCorrectISBN(String isbn) throws Exception {
        Book book = getSimpleBook();
        book.setIsbn(isbn);

        mockMvc.perform(post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_DeleteBook() throws Exception {
        mockMvc.perform(delete("/books/" + CORRECT_ISBN_10))
                .andExpect(status().isNoContent());
    }

    @Test
    void should_UpdateBook() throws Exception {
        Book book = getSimpleBook();
        mockMvc.perform(put("/books/" + book.getIsbn())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isOk());
    }

    @Test
    void should_ReturnBadRequestStatus_When_IncorrectISBN() throws Exception {
        Book book = getSimpleBook();
        book.setIsbn(INCORRECT_ISBN);

        mockMvc.perform(post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void should_ReturnBadRequestStatus_When_RatingOutOfRange() throws Exception {
        Book book = getSimpleBook();
        book.setRating(6);

        mockMvc.perform(post("/books")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(book)))
                .andExpect(status().isBadRequest());
    }
}