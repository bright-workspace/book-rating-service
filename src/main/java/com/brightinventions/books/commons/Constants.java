package com.brightinventions.books.commons;

public class Constants {
    public static final int MIN_RATING = 1;
    public static final int MAX_RATING = 5;
    public static final String ISBN_10_PATTERN = "^(?=[0-9X]{10}$|(?=(?:[0-9]+[- ]){3})[- 0-9X]{13}$)[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9X]$";
    public static final String ISBN_13_PATTERN = "^(?=[0-9]{13}$|(?=(?:[0-9]+[- ]){4})[- 0-9]{17}$)97[89][- ]?[0-9]{1,5}[- ]?[0-9]+[- ]?[0-9]+[- ]?[0-9]$";
    public static final String ISBN_PATTERN = ISBN_10_PATTERN + "|" + ISBN_13_PATTERN;
    public static final String AUTHOR_PATTERN = "^[\\p{L}\\s.’\\-,]+$";
}
