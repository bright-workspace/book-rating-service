package com.brightinventions.books.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ErrorResponse {

    @Data
    @AllArgsConstructor
    private class Error{
        private String message;
    }

    public ErrorResponse(String message){
        error = new Error(message);
    }

    private Error error;
}
