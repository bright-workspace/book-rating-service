package com.brightinventions.books.controller;

import com.brightinventions.books.model.Book;
import com.brightinventions.books.service.BookService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Validated
@RequestMapping("/books")
public class BookController {

    private final BookService readingHistoryService;

    public BookController(final BookService readingHistoryService) {
        this.readingHistoryService = readingHistoryService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Book> getBooks(){
        return readingHistoryService.getAll();
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveBook(@RequestBody @Valid Book book){
        readingHistoryService.save(book);
    }

    @PutMapping(path = "/{isbn}", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Book updateBook(@RequestBody @Valid Book book, @PathVariable @Valid String isbn) {
        return readingHistoryService.update(isbn, book);
    }

    @PatchMapping(path = "/{isbn}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Book patchBook(@RequestBody Book book, @PathVariable @Valid String isbn) {
        return readingHistoryService.update(isbn, book);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(path = "/{isbn}")
    public void deleteBook(@PathVariable @Valid String isbn){
        readingHistoryService.remove(isbn);
    }
}
