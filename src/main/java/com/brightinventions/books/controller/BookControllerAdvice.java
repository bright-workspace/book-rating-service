package com.brightinventions.books.controller;

import com.brightinventions.books.exception.BookAlreadyExistsException;
import com.brightinventions.books.exception.BookDoesNotExistException;
import com.brightinventions.books.exception.ErrorResponse;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static org.assertj.core.util.Throwables.getRootCause;


@RestControllerAdvice
public class BookControllerAdvice {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleMongoServiceException(MethodArgumentNotValidException ex){
        return new ErrorResponse("Invalid field value: " + ex.getBindingResult().getFieldError().getField());
    }

    @ExceptionHandler(InvalidFormatException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleInvalidFormatException(InvalidFormatException ex){
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(BookDoesNotExistException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorResponse handleNoSuchBookInDatabaseException(BookDoesNotExistException ex){
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(BookAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleBookAlreadyExistsException(BookAlreadyExistsException ex){
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(JsonParseException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleJsonParseException(JsonParseException ex){
        return new ErrorResponse(ex.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse handleException(Exception ex){
        return new ErrorResponse(getRootCause(ex).getMessage());
    }
}
