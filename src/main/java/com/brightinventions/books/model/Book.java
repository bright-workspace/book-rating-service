package com.brightinventions.books.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import static com.brightinventions.books.commons.Constants.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "book")
@Entity
public class Book {
    @Id
    @NotNull
    @Pattern(regexp = ISBN_PATTERN)
    private String isbn;

    @NotNull
    private String title;

    @NotNull
    @Pattern(regexp = AUTHOR_PATTERN)
    private String author;

    @NotNull
    @Min(1)
    private Integer pages;

    @NotNull
    @Range(min = MIN_RATING, max = MAX_RATING)
    private Integer rating;
}
