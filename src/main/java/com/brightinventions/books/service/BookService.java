package com.brightinventions.books.service;

import com.brightinventions.books.exception.BookAlreadyExistsException;
import com.brightinventions.books.exception.BookDoesNotExistException;
import com.brightinventions.books.model.Book;
import com.brightinventions.books.repository.BookRepository;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

@Service
public class BookService {

    private final BookRepository bookRepository;

    public BookService(final BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    public List<Book> getAll() {
        return bookRepository.findAll();
    }

    public void save(final Book newBook) {
        Book book = bookRepository.findById(newBook.getIsbn()).orElse(null);

        if(nonNull(book)){
            throw new BookAlreadyExistsException("Book with isbn code: " + newBook.getIsbn() + " already exist");
        }
        bookRepository.save(newBook);
    }

    public Book findByIsbn(final String isbn){
        return bookRepository.findById(isbn)
                .orElseThrow(() -> new BookDoesNotExistException("Book with isbn code: " + isbn + " doesn't exist in database"));
    }

    public Book update(final String isbn, final Book modifiedBook) {

        Book book = findByIsbn(isbn);
        book.setAuthor(isNull(modifiedBook.getAuthor()) ? book.getAuthor() : modifiedBook.getAuthor());
        book.setPages(isNull(modifiedBook.getPages()) ? book.getPages() : modifiedBook.getPages());
        book.setRating(isNull(modifiedBook.getRating()) ? book.getRating() : modifiedBook.getRating());
        book.setAuthor(isNull(modifiedBook.getAuthor()) ? book.getAuthor() : modifiedBook.getAuthor());
        book.setTitle(isNull(modifiedBook.getTitle()) ? book.getTitle() : modifiedBook.getTitle());

        return bookRepository.save(book);
    }

    public void remove(final String isbn) {
        bookRepository.delete(findByIsbn(isbn));
    }
}
