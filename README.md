# README #

Book rating service API.

## Installation steps ##

1. Clone the repository:
    ```
    git clone https://szefkosmosu@bitbucket.org/bright-workspace/book-rating-service.git
    ```
    
2. Enter the project directory and run:
    ```
    mvn install
    ```  

3. Execute jar:
    ```
    java -jar target/book-rating-service-1.0.0-SNAPSHOT-all.jar
    ```  

## Usage ##

### GET method: ###
```
/books
```

Example Response: 
```
[
    {
        "isbn": "0-545-01022-1",
        "title": "Random Title",
        "author": "Dummy Author",
        "pages": 1,
        "rating": 2
    },
    {
        "isbn": "0-545-01022-2",
        "title": "Random Title",
        "author": "Dummy Author",
        "pages": 1,
        "rating": 2
    }
]
```

### POST METHOD: ###
```
/books
```

Example request body:
```
{
    "isbn": "0-545-01022-1",
    "title": "Random Title",
    "author": "Dummy Author",
    "pages": 1,
    "rating": 2
}
```

### PUT METHOD: ###
```
/books/{isbn}
```

Example request body:
```
{
    "isbn": "0-545-01022-1",
    "title": "Random Title",
    "author": "Dummy Author",
    "pages": 1,
    "rating": 4
}
```

### PATCH METHOD: ###
```
/books/{isbn}
```

Example request body:
```
{
    "rating": 5
}
```

DELETE METHOD:
```
/books/{isbn}
```